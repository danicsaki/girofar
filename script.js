


var a;
function startSirens() {
    var audio = document.querySelector('#aud');
    var one = document.querySelector('#one');
    one.style.backgroundColor = "red";
    var two = document.querySelector('#two');
    two.style.backgroundColor = "blue";
    audio.play();
    var auxiliar = one.style.backgroundColor;
    one.style.backgroundColor = two.style.backgroundColor;
    two.style.backgroundColor = auxiliar;
    a = setTimeout(startSirens, 100);
}

function stopSirens() {
    var audio = document.querySelector('#aud');
    clearTimeout(a);
    audio.pause();
}